import { Component, OnInit, Input, OnChanges, AfterViewChecked, AfterContentInit } from '@angular/core';

@Component({
    selector: 'app-all-shows',
    templateUrl: './all-shows.component.html',
    styles: [``]
})
export class AllShowsComponent implements OnInit, OnChanges, AfterViewChecked, AfterContentInit {

    @Input() allShows: any[];

    constructor() {}

    ifSorted: boolean = false;
    groupedShows: any[]
    nullArray: any[] = new Array;
    pastArray: any[] = new Array;
    futureArray: any[] = new Array;
    today: any = new Date()

    ngOnInit() {

        /*console.log(this.allShows)

        console.log(JSON.stringify(this.allShows))
        console.log(JSON.stringify(this.sortArrays(this.allShows)))*/
    }

    ngOnChanges() {
        console.log("On Change")
        console.log(this.allShows)

        console.log(JSON.stringify(this.allShows))
        console.log(JSON.stringify(this.sortArrays(this.allShows)))
    }

    ngAfterViewChecked(){
        //console.log(this.allShows)
        //this.groupedShows = this.sortArrays(this.allShows)
    }
    
    onclick(){
        console.log(this.groupedShows)
    }
    ngAfterContentInit(){}

    sortArrays(array: any[]) {
        console.log(array)

        for (let item of array) {
            let itemDate = new Date(item.schedule.premiereDate)
            if (itemDate == null) {
                this.nullArray.push(item)
            } else if (itemDate < this.today) {
                this.pastArray.push(item)
            } else {
                this.futureArray.push(item)
            }
        }

        return [this.futureArray, this.nullArray, this.pastArray]
    }

}
