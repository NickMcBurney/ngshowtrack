import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';


import { AppComponent } from './app.component';

import { MomentModule } from 'angular2-moment';
import { TvShowComponent } from './tv-show/tv-show.component';
import { AllShowsComponent } from './all-shows/all-shows.component';
import { SortDatePipe } from './sort.pipe';
import { TvMazeService } from './tvmaze.service';


@NgModule({
  declarations: [
    AppComponent,
    TvShowComponent,
    AllShowsComponent,
    SortDatePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    MomentModule,
  ],
  providers: [TvMazeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
