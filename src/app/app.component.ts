import { Component, OnInit, OnChanges } from '@angular/core';
import { TvMazeService } from './tvmaze.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges {



    allshows: any = [];
    showsObject: any;

    constructor(private tvMazeService: TvMazeService) {
        //this.showsObject = this.tvMazeService.buildShowArray(this.allshows, this.showDemoJson2)
        this.showsObject = this.tvMazeService.buildShowArray(new Array, this.showsJson)
    }

    allImgs: any;
    theImage: string
    ngOnInit() {
    }


    ngOnChanges(){
        console.log("data ready")
    }

    showsJson: any[] = [
        "Rick and Morty",
        "South Park",
        "It's Always Sunny In Philadelphia",
        "RuPaul's Drag Race",
        "Silent Witness",
        "Better Call Saul",
        "The Walking Dead"
    ]
}
