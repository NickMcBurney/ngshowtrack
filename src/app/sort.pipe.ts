import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'orderByDate', pure: false})

export class SortDatePipe {

  transform(array: Array<Object>, args: string): Array<Object> {
    if (array == null) {
      return null;
    }

    // sort array using dates
    array.sort(function(a,b){

      // variables
      let today:any = new Date()
      let aDate:any = new Date(a[args].premiereDate)
      let bDate:any = new Date(b[args].premiereDate)
      
      // send null dates to last position
      if(a[args].premiereDate === null){
        return 1;
      }
      else if (b[args].premiereDate === null){
        return -1;
      }

      // send already passed dates to last position
      if (aDate < today){
        return 1;
      }
      else if(bDate < today){
        return -1;
      }

      
      
      // sort future dates into soonest first
      return aDate - bDate;
    });
    // return sorted array
    return array;
  }
}