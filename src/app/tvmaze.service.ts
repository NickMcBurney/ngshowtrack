import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';

@Injectable()
export class TvMazeService {
  constructor(private _http: Http) { }


  searchTvShow(term) {
    //term = encodeURIComponent(term.trim())
    return this._http.get('http://api.tvmaze.com/search/shows?q=' + term).map(data => data.json())
  }

  getSeries(id) {
    return this._http.get('http://api.tvmaze.com/shows/' + id + '/seasons').map(data => {
      // the series schedule object
      let scheduleObject = data.json()
      // get the latest series
      scheduleObject = scheduleObject[scheduleObject.length - 1]
      //console.log(scheduleObject)
      return scheduleObject
    })
  }

  buildShowArray(arrayName, shows) {
    
    //for (let show of shows) {
    shows.forEach(show => {
    
      return this._http.get('http://api.tvmaze.com/search/shows?q=' + show).subscribe(data => {
        let showObject = data.json()


        this._http.get('http://api.tvmaze.com/shows/' + showObject[0].show.id + '/seasons').subscribe(data => {
          // the series schedule object
          let scheduleObject = data.json()
          // get the latest series
          scheduleObject = scheduleObject[scheduleObject.length - 1]

          showObject.push(scheduleObject)

          let showInfomation = {
            'show': showObject[0],
            'schedule': showObject[showObject.length - 1]
          }
          arrayName.push(showInfomation)
        })
      })
    })
    console.log(arrayName)
    //console.log(sObj)
    let newShowArray: Observable<any[]> = arrayName
    return newShowArray
  }


}